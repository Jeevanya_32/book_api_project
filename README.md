Postman is an API testing tool for SOAP and REST API ,designed to send requests from client to web server and receive response from backend.

**Collections** 

a)Trigger API and receive response

b)Using Postman library,we write our custom testscripts

c)To run and test API collections by using collection runner

Authorisation - API uses token based authorisation via headers.

**REQUEST** - 

It allow user to store API request of each API  for different HTTP methods(GET,POST,PUT,PATCH,DELETE).

**HTTPS Methods**

GET -retrieves data from specified resource

POST-Create new data in a resource

PUT-replace the  entire resource

PATCH- modify a specified resource

DELETE-removes data from a resource

Test Script - write and run test for each API request in Javascript language using postman library.

RESPONSE - It consists of body,headers and statuscode and test results.

**Variables**

Variables in postman can be reused in multiple places based on its usage.

Types of variables in postman are,

1.**Local varibale** - Low applicability 

2.**Collection varibale** - Applicable for entire collection (Multiple API)

3.**Environment varibale** - By declaring the base url {{Base_URL}},all environment related data are stored

4.**Global varibale** - Applicable for entire postman workspace

**Data Driven Testing**
 

 To run API and testing code on fixed set of data 

We can use CSV or JSON format files.

**Reports**

In postman,by exporting the collection files,environment files and data files we can generate reports of html and htmlextra using Newman(command line interface tool). 

**Benefits of postman:**

1)User friendly

2)Easily export our collection and move to code repository

3)Effective integration with jenkins by CI/CD

4)Execute test in iterations


